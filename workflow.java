/**
 * Task in task tracking system.
 */
public class Task {
  //*** instance fields
  private String  description, owner;
  private Date    created, updated;

  //*** constructors
  // tag::Task[]
  /**
   * Creates Task with given values.
   * @param description
   * @param owner
   */
  public Task(String description, String owner) {
    this.description  = description;
    this.owner        = owner;
    this.created      = new Date();
    this.updated      = new Date();
  }
  // end::Task[]

  //*** methods
  // tag::toString[]
  /**
   * Returns Task as String.
   * @return  Task as String.
   */
  public String toString() {
    return this.description + "[" + this.owner + "]";
  }
  // end::toString[]

} // end class
